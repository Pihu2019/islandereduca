//
//  ProfileViewController.m
//  Eshiksa
//
//  Created by Punit on 19/04/18.
//  Copyright © 2018 Akhilesh. All rights reserved.
//

#import "ProfileViewController.h"
#import <Foundation/Foundation.h>
#import "Constant.h"
#import "BaseViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Base.h"
@interface ProfileViewController ()
{
    BOOL isSelected;
}

@end

@implementation ProfileViewController
@synthesize studentIdtext;
@synthesize groupnametxt;
@synthesize dateofbirthtxt;
@synthesize addresstxt;
@synthesize emailtxt;
@synthesize mobiletxt;
@synthesize aboutTxt;
@synthesize contactTxt;

- (void)viewDidLoad {
    [super viewDidLoad];
    
   
  
    self.profilepicImg.layer.cornerRadius = self.profilepicImg.frame.size.width / 2;
    self.profilepicImg.clipsToBounds = YES;
    //self.profilepicImg.layer.borderWidth = 1.0f;
    //self.profilepicImg.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
//    self.profileview.layer.cornerRadius = self.profileview.frame.size.width / 2;
//    self.profileview.clipsToBounds = YES;
   // self.profileview.layer.borderWidth = 1.0f;
    //self.profileview.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    //self.upBtn.layer.cornerRadius=6.0f;
    //self.downBtn.layer.cornerRadius=6.0f;
    
   
    UIBezierPath *maskPath1 = [UIBezierPath bezierPathWithRoundedRect:_upBtn.bounds
                                                   byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight)
                                                         cornerRadii:CGSizeMake(6.0, 6.0)];
    
    // Create the shape layer and set its path
    CAShapeLayer *maskLayer1 = [CAShapeLayer layer];
    maskLayer1.frame = _upBtn.bounds;
    maskLayer1.path = maskPath1.CGPath;
    // Set the newly created shapelayer as the mask for the image view's layer
    _upBtn.layer.mask = maskLayer1;
    
    UIBezierPath *maskPath2 = [UIBezierPath bezierPathWithRoundedRect:_downBtn.bounds
                                                    byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight)
                                                          cornerRadii:CGSizeMake(6.0, 6.0)];
    
    // Create the shape layer and set its path
    CAShapeLayer *maskLayer2 = [CAShapeLayer layer];
    maskLayer2.frame = _downBtn.bounds;
    maskLayer2.path = maskPath2.CGPath;
    // Set the newly created shapelayer as the mask for the image view's layer
    _downBtn.layer.mask = maskLayer2;
    
    
    [self getProfile];
    
}


-(void)getProfile{
    
    NSString *groupname = [[NSUserDefaults standardUserDefaults]
                           stringForKey:@"groupName"];
    NSLog(@"group name in profile==%@",groupname);
    
    NSString *password = [[NSUserDefaults standardUserDefaults]
                           stringForKey:@"password"];
    NSLog(@"profile password ==%@",password);
    
    NSString *orgid = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"orgid"];
    NSLog(@"profile orgid ==%@",orgid);
    
    NSString *branchid = [[NSUserDefaults standardUserDefaults]
                       stringForKey:@"branchid"];
    NSLog(@"profile branchid ==%@",branchid);
    
    NSString *cyear = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"cyear"];
    NSLog(@"profile cyear ==%@",cyear);
    
    NSString *username = [[NSUserDefaults standardUserDefaults]
                       stringForKey:@"username"];
    NSLog(@"profile username ==%@",username);
    
    NSString *mainstr1=[NSString stringWithFormat:@"%@",[mainUrl stringByAppendingString:profile]];
    
    
    NSDictionary *parameterDict =
    @{ @"groupname":groupname,
       @"username": username,
       @"dbname":dbname,
       @"Branch_id":branchid,
       @"org_id":orgid,
       @"cyear":cyear,
       @"url": mainstr1,
       @"tag": @"user_detail",
       @"password": password };
    
    NSLog(@"parameter dict%@",parameterDict);
    
    [Constant executequery:mainstr1 strpremeter:parameterDict withblock:^(NSData * dbdata, NSError *error) {
        NSLog(@"data:%@",dbdata);
        if (dbdata!=nil) {
            NSDictionary *maindic=[NSJSONSerialization JSONObjectWithData:dbdata options:NSJSONReadingAllowFragments error:nil];
            NSLog(@"response teacher profile data:%@",maindic);
            
            self.empid.text=[maindic objectForKey:@"empId"];
            self.groupname.text=[maindic objectForKey:@"groupname"];
            self.mobile.text=[maindic objectForKey:@"mobile"];
            self.email.text=[maindic objectForKey:@"email"];
            self.dob.text=[maindic objectForKey:@"dob"];
            self.address.text=[maindic objectForKey:@"address"];
            self.firstName.text=[maindic objectForKey:@"first_name"];
            self.lastName.text=[maindic objectForKey:@"last_name"];
    
            NSString *str4=[maindic objectForKey:@"pic_id"];
               NSString *str = [instUrl stringByAppendingString:str4];
            
            if(str==(NSString *) [NSNull null])
            {
                str=@"-";
            }
            if(str4==(NSString *) [NSNull null])
            {
                str4=@"-";
            }
            if(self.empid.text==(NSString *) [NSNull null])
            {
                self.empid.text=@"-";
            }
            if(self.mobile.text==(NSString *) [NSNull null])
            {
                self.mobile.text=@"-";
            }
            if(self.email.text==(NSString *) [NSNull null])
            {
                self.email.text=@"-";
            }
            if(self.dob.text==(NSString *) [NSNull null])
            {
                self.dob.text=@"-";
            }
            if(self.address.text==(NSString *) [NSNull null])
            {
                self.address.text=@"-";
            }
            if(self.firstName.text==(NSString *) [NSNull null])
            {
                self.firstName.text=@"-";
            }
            if(self.lastName.text==(NSString *) [NSNull null])
            {
                self.lastName.text=@"-";
            }
            NSString *tempimgstr=str;
           [_profilepicImg sd_setImageWithURL:[NSURL URLWithString:tempimgstr]
                       placeholderImage:[UIImage imageNamed:@"default.png"]];
            
            self.teacherName.text = [NSString stringWithFormat: @"%@ %@", self.firstName.text,self.lastName.text];
            
            
            [[NSUserDefaults standardUserDefaults] setObject:self.teacherName.text forKey:@"teachername"];
            [[NSUserDefaults standardUserDefaults] synchronize];
                NSLog(@"teachername ==%@",self.teacherName.text);
            
            [[NSUserDefaults standardUserDefaults] setObject:self.email.text forKey:@"email"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            NSLog(@"email ==%@",self.email.text);
            
            
            NSLog(@"Emp-id====%@ mobile num==%@",self.empid.text,self.mobile.text);
        }
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewDidLoad];
    
    
    studentIdtext.text = [@"STUDENT_ID" localize];
    dateofbirthtxt.text=[@"DOB" localize];
    addresstxt.text=[@"HOME_ADDRESS" localize];
    emailtxt.text=[@"HOME_EMAIL" localize];
    groupnametxt.text=[@"BATCH" localize];
    mobiletxt.text=[@"HOME_MOBILE" localize];
    aboutTxt.text=[@"ABOUT" localize];
    contactTxt.text=[@"CONTACT" localize];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeLanguage:) name:@"notificationName" object:nil];
}

-(void)changeLanguage:(NSNotification*)notification
{
    studentIdtext.text = [@"STUDENT_ID" localize];
    dateofbirthtxt.text=[@"DOB" localize];
    addresstxt.text=[@"HOME_ADDRESS" localize];
    emailtxt.text=[@"HOME_EMAIL" localize];
    groupnametxt.text=[@"BATCH" localize];
    mobiletxt.text=[@"HOME_MOBILE" localize];
    aboutTxt.text=[@"ABOUT" localize];
    contactTxt.text=[@"CONTACT" localize];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
