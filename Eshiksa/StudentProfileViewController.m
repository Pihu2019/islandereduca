//
//  StudentProfileViewController.m
//  Eshiksa
//
//  Created by Punit on 24/04/18.
//  Copyright © 2018 Akhilesh. All rights reserved.


#import "StudentProfileViewController.h"
#import <Foundation/Foundation.h>
#import "Constant.h"
#import "BaseViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Base.h"
@interface StudentProfileViewController ()


@end

@implementation StudentProfileViewController
@synthesize admissionNumtext;
@synthesize sectiontxt;
@synthesize coursetxt;
@synthesize dateofbirthtxt;
@synthesize addresstxt;
@synthesize emailtxt;
@synthesize batchtxt;
@synthesize mobiletxt;
@synthesize aboutTxt;
@synthesize contactTxt;

- (void)viewDidLoad {
    [super viewDidLoad];
   
  self.profilepicImg.layer.cornerRadius = self.profilepicImg.frame.size.width / 2;
    self.profilepicImg.clipsToBounds = YES;
//    self.profilepicImg.layer.borderWidth = 1.0f;
//    self.profilepicImg.layer.borderColor = [UIColor lightGrayColor].CGColor;
//    
//    self.profileview.layer.cornerRadius = self.profileview.frame.size.width / 2;
//    self.profileview.clipsToBounds = YES;
    //self.profileview.layer.borderWidth = 1.0f;
    //self.profileview.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
 

    UIBezierPath *maskPath1 = [UIBezierPath bezierPathWithRoundedRect:_upBtn.bounds
                                                    byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight)
                                                          cornerRadii:CGSizeMake(6.0, 6.0)];
    
    // Create the shape layer and set its path
    CAShapeLayer *maskLayer1 = [CAShapeLayer layer];
    maskLayer1.frame = _upBtn.bounds;
    maskLayer1.path = maskPath1.CGPath;
    // Set the newly created shapelayer as the mask for the image view's layer
    _upBtn.layer.mask = maskLayer1;
    
    UIBezierPath *maskPath2 = [UIBezierPath bezierPathWithRoundedRect:_downBtn.bounds
                                                    byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight)
                                                          cornerRadii:CGSizeMake(6.0, 6.0)];
    
    // Create the shape layer and set its path
    CAShapeLayer *maskLayer2 = [CAShapeLayer layer];
    maskLayer2.frame = _downBtn.bounds;
    maskLayer2.path = maskPath2.CGPath;
    // Set the newly created shapelayer as the mask for the image view's layer
    _downBtn.layer.mask = maskLayer2;
    
    
    [self getProfile];

}

-(void)getProfile{
    
    NSString *groupname = [[NSUserDefaults standardUserDefaults]
                           stringForKey:@"groupName"];
    NSString *username = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"username"];
    NSString *branchid = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"branchid"];
    
    NSString *orgid = [[NSUserDefaults standardUserDefaults]
                       stringForKey:@"orgid"];
    NSString *cyear = [[NSUserDefaults standardUserDefaults]
                       stringForKey:@"cyear"];
    NSString *password = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"password"];

    NSString *mainstr1=[NSString stringWithFormat:@"%@",[mainUrl stringByAppendingString:profile]];
   
    
    NSDictionary *parameterDict =
    @{ @"groupname": groupname,
       @"username": username,
       @"dbname":dbname,
       @"Branch_id": branchid,
       @"org_id": orgid,
       @"cyear": cyear,
       @"url": mainstr1,
       @"tag": @"user_detail",
       @"password": password};
    
    
    [Constant executequery:mainstr1 strpremeter:parameterDict withblock:^(NSData * dbdata, NSError *error) {
        NSLog(@"data:%@",dbdata);
        if (dbdata!=nil) {
            NSDictionary *maindic=[NSJSONSerialization JSONObjectWithData:dbdata options:NSJSONReadingAllowFragments error:nil];
            NSLog(@"response student profile data:%@",maindic);
            
            self.studentId.text=[maindic objectForKey:@"studentId"];
            self.groupname.text=[maindic objectForKey:@"groupname"];
            self.mobile.text=[maindic objectForKey:@"mobile"];
            self.email.text=[maindic objectForKey:@"email"];
            NSString *dob=[maindic objectForKey:@"dob"];
            self.address.text=[maindic objectForKey:@"address"];
            self.firstName.text=[maindic objectForKey:@"first_name"];
            self.lastName.text=[maindic objectForKey:@"last_name"];
            self.instUrl.text=[[maindic objectForKey:@"instUrl"]description];
            self.course.text=[maindic objectForKey:@"course_name"];
            self.batch.text=[maindic objectForKey:@"batch_name"];
            self.semestername.text=[maindic objectForKey:@"semester_name"];
            self.section.text=[[maindic objectForKey:@"section_name"]description];
            self.username.text=[maindic objectForKey:@"username"];
            
            NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
            formatter.dateFormat=@"dd-MM-YYYY";
           dob=[formatter stringFromDate:[NSDate date]];
            NSLog(@"******dob1**%@",dob);
            self.dob.text=dob;
            
            NSString *str4=[maindic objectForKey:@"pic_id"];
            NSLog(@"******pic_id**%@",str4);
            
            NSString *str = [instUrl stringByAppendingString:str4];
            
            if(self.section.text==(NSString *) [NSNull null])
            {
                self.section.text=@"-";
            }
            if(self.semestername.text==(NSString *) [NSNull null])
            {
             self.semestername.text=@"-";
            }
            if(self.instUrl.text==(NSString *) [NSNull null])
            {
                self.instUrl.text=@"-";
            }
            if(self.dob.text==(NSString *) [NSNull null])
            {
                self.dob.text=@"-";
            }
            if(self.address.text==(NSString *) [NSNull null])
            {
                self.address.text=@"-";
            }
   
            NSString *tempimgstr=str;
           [_profilepicImg sd_setImageWithURL:[NSURL URLWithString:tempimgstr]
                              placeholderImage:[UIImage imageNamed:@"default.png"]];
            
            self.studentName.text = [NSString stringWithFormat: @"%@ %@", self.firstName.text,self.lastName.text];
            
            NSLog(@"studentId====%@ mobile num==%@",self.studentId.text,self.mobile.text);
            
            [[NSUserDefaults standardUserDefaults] setObject:_studentId.text forKey:@"studentId"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }];

}



- (void)viewWillAppear:(BOOL)animated {
    [super viewDidLoad];
    

    admissionNumtext.text = [@"STUDENT_ID" localize];
    sectiontxt.text=[@"SECTION" localize];
    coursetxt.text = [@"COURSE" localize];
    dateofbirthtxt.text=[@"DOB" localize];
    addresstxt.text=[@"HOME_ADDRESS" localize];
    emailtxt.text=[@"HOME_EMAIL" localize];
    batchtxt.text=[@"BATCH" localize];
    mobiletxt.text=[@"HOME_MOBILE" localize];
    aboutTxt.text=[@"ABOUT" localize];
    contactTxt.text=[@"CONTACT" localize];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeLanguage:) name:@"notificationName" object:nil];
}

-(void)changeLanguage:(NSNotification*)notification
{
    admissionNumtext.text = [@"STUDENT_ID" localize];
    sectiontxt.text=[@"SECTION" localize];
    coursetxt.text = [@"COURSE" localize];
    dateofbirthtxt.text=[@"DOB" localize];
    addresstxt.text=[@"HOME_ADDRESS" localize];
    emailtxt.text=[@"HOME_EMAIL" localize];
    batchtxt.text=[@"BATCH" localize];
    mobiletxt.text=[@"HOME_MOBILE" localize];
    aboutTxt.text=[@"ABOUT" localize];
    contactTxt.text=[@"CONTACT" localize];
}

- (void)viewDidLayoutSubviews{
    NSString *language = [@"" currentLanguage];
    if ([language isEqualToString:@"hi"])
    {
        [self setBackButtonLocalize];
    }
}

- (void)setBackButtonLocalize{
    self.navigationItem.title = [@"MY_PROFILE" localize];
}

@end
