//
//  FeesTableViewCell.h
//  Islanders Education
//
//  Created by Punit on 21/11/18.
//  Copyright © 2018 Akhilesh. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FeesTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *feesName;
@property (weak, nonatomic) IBOutlet UILabel *dueAmount;
@property (weak, nonatomic) IBOutlet UIButton *selectBtn;

@end

NS_ASSUME_NONNULL_END


/*
 
 -(void)payNowBtnClicked:(UIButton *)sender{
 
 UIButton *aButton = (UIButton *)sender;
 NSLog(@" payNowBtnClicked indexPath.row: %ld",aButton.tag);
 [self pgParamsDataParsing];
 
 
 
 }
 */
